package kumori 

import (
  "encoding/json"
  u "kumori.systems/utils"
)


_SpreadR:  (u.#RecurseN & {#N: 10, #funcFactory: _SpreadF}).Func

_SpreadF: {
  #next: _ 

  func: {
    $p=#p: _

    out: {...}
    _out: {...}

    
    let data = json.Unmarshal(json.Marshal($p.config))
    let first = $p.artifact & {description: config: data}


    let $in = first.description

    _out: spec: first.spec
    _out: ref:  first.ref

    
    if $in.builtin {
      _out: description: $in
    }

    if !$in.builtin && first.ref.kind == "component"  {
      _out: description: (#ComponentCompleteDefaults & {#d: $in}).#out
    }

    if first.ref.kind == "service" && !$in.builtin {
      let copyfields_a  = or(["srv", "vset", "connect", "builtin"])
      let copyfields_ac = or(["resource", "scale", "resilience"])

      //let copyfields_r = or(["artifact", "name", "meta", "up"])
      let copyfields_rc = copyfields_ac

      // For some reason it cannot be copied as part of the list
      
      // Copy untransformed fields
      _out: description: {
        for k, v in $in if (k & copyfields_a) != _|_ {"\(k)": v}
        for k, v in $in.config if (k & copyfields_ac) != _|_ {config: "\(k)": v}  
        // filter out the parameters
        config: parameter: {...}
      }    
      
      for rn, rnd in $in.role {
        let data = json.Unmarshal(json.Marshal({config: {
            parameter: rnd.config.parameter
            resource : rnd.config.resource
            scale    : rnd.config.scale
            if rnd.artifact.ref.kind == "service" {
              resilience: rnd.config.resilience
            }
            if rnd.artifact.ref.kind != "service" {
              resilience: 0
            }
        }}))

        let payload = {
          artifact: rnd.artifact
          data
        }

        let newartifact = (#next & {#p: payload}).out

        _out: description: role: "\(rn)": {
          artifact: newartifact
          meta    : rnd.meta
          name    : rnd.name
          up      : rnd.up
          for k, v in rnd.config if (k & copyfields_rc) != _|_ {config: "\(k)": v}  
        }
      }
    }
    // We must have pure data after the spread
    out: json.Unmarshal(json.Marshal(_out))
  }
}

#Spread: {
  $p=#p: _ 

  let almost = ((_SpreadR & {#p: $p}).out) &  {description: config: parameter: $p.config.parameter}
  let refresh = json.Unmarshal(json.Marshal(almost)) // Incredible, but doing this is faster!!

  out: refresh
}
