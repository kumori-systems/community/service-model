@if(cspec10||!cspec10)

package kumori

import (
  "list"
  "encoding/json"
  u "kumori.systems/utils:utils"
)

#ServiceFromComponentDeployment: {
  $p=#p: _ 
  _art: #ServiceArtifact & {
    spec: $p.artifact.spec 
    ref: {
      for k, v in $p.artifact.ref if k != "kind" {
        "\(k)": v
      }
      kind: "service"
    }
    description: {
      let rolename = $p.name
      role: "\(rolename)": $p

      for k, v in $p.artifact.description.srv.server {
        srv: server: "\(k)": v
        connect: "s-\(k)": {
          as  : "lb"
          to  : "\(rolename)": "\(k)": _
          from: self         : k
        }
      }
      for k, v in $p.artifact.description.srv.client {
        srv: client: "\(k)": v
        connect: "c-\(k)": {
          as  : "lb"
          from: "\(rolename)": k
          to  : self         : "\(k)": _
        }
      }
      for k, v in $p.artifact.description.srv.duplex {
        connect: "d-\(k)": {
          as: "full"
          to: "\(rolename)": "\(k)": _
        }
      }
    }
  }
  out: #Deployment & {
    name: $p.name
    artifact: _art
  }
}

// Produce a ServiceArtifact out of a ComponentArtifact
//  A service ref with same fields is used
//  Config is the same.
//  Only one Role, with the name of the component, is created
//  cfg  picks up the entire service Config
//  server channels copied to service server channels, connectoed to the role via an LB
//  client channels copied to service client channels, to which role channels get connected through a LB
//  duplex channels remain internal, but get conected through full connectors to them
#ServiceFromComponent: {
  $p=#p: {spec: [uint, uint], ref: {kind: "component", ...}, description: {...}}
  out: #ServiceArtifact & {
    let rname = $p.ref.name 

    spec: $p.spec
    ref: {
      for k, v in $p.ref if k != "kind" {
        "\(k)": v
      }
      kind: "service"
    }
    description: {
      let oconf = $p.description.config
      vin = config: {
        parameter: oconf.parameter
        resource:  oconf.resource 
      }

      // Copy and connect servers
      for cn, cd in $p.description.srv.server {
        srv: server: "\(cn)": cd
        connect: "s_\(cn)": {
          as: "lb"
          from: self: cn 
          to: "\(rname)": "\(cn)": _
        }
      }
      // Copy and connect clients
      for cn, cd in $p.description.srv.client {
        srv: client: "\(cn)": cd  
        connect: "c_\(cn)": {
          as: "lb"
          from: "\(rname)": cn 
          to: self: "\(cn)": _
        }
      }
      // full-connect duplexes
      for cn, cd in $p.description.srv.duplex {
        connect: "f_\(cn)": {
          as: "full"
          to: "\(rname)": "\(cn)": _
        }
      }
      role: "\(rname)": {
        artifact: $p 
        config: {
          parameter: vin.parameter 
          resource:  vin.resource
          resilience: vin.resilience
        }
      }
    }
  }
}



// Computes the connectors as expected by the output
// It assumes that the connect dictionary involves only
// roles, no vsets, and the meta of the to field is the metadata
// to be considered for the tag represented by each
// target pari <role,channel>

// NOTE: that the representation allows for multiple role/channel 
// within the same target tag, but we are not currently taking advantage of that
//
// NOTE: MULTIPORT server channels are being ignored (EXPERIMENTAL FEATURE for now)
//
// TB INTERNAL
#ComputeConnectors: {
  $p=#p: service : _//_Service

  // Reverse mapping self-channel -- connector from it
  fromself: {...}

  // Reverse mapping self-channel(s) -- connector to them
  toself: {...}

  for cn, cnd in $p.service.connect  {
    if cnd.from.self != _|_ {
      for ch in cnd.from.self {
        fromself: "\(ch)": cn
      }
    }
    if cnd.to.self != _|_ {
      for ch, rest in cnd.to.self {
        toself: "\(ch)": cn: rest  // WARNING!! This is not being employed, but this way it allows multiple cns
      }
    }
  }

  connector: {...}

  // Now we traverse the set of connectors to complete their
  // info based on the links specified in the service
  // We consider aonly those connectors that are linked
  for cn, cd in #p.service.connect if (len(cd.from) > 0 || cd.as == "full") && len(cd.to) > 0 {

    connector: "\(cn)": {
      kind: cd.as

      clients: list.FlattenN([
        for rn, chnl in cd.from {
          [for chn in chnl {
              {
                role: rn
                channel: chn
              }
          }]
        }
      ], -1)
      
 
      servers: [...]

      // We collect the links out to create the tags list
      // and, within it, the target enpoints (servers)
      servers: list.FlattenN([for rn, rchset in cd.to {
        
          // Each target role  contributes a tag
          [for rch, rchm in rchset {   
            meta: [ 
              {
                // The metadata for a tag has two fiels, an auto field, and a user field
                // Note that the metadata is a List (actually a stack) we push metadata 
                // provided by enclosing subservices/deployemnts. The bottom of the stack
                // contains the metadata provided by the component role implementing
                // the actual microservice.
                auto: {
                  // The automatic part of the metadata captures info about the artifact
                  // be it a service app or a component.
                  // It also captures the role name.
                  // Note that no auto info is provided when the role is `"\($self)"`
                  if rn != "self" {
                    compRef: #p.service.role[rn].artifact.ref
                  }
                  compRef?: {...}
                  roleName: rn
                }
                user: rchm.meta
              }
            ]

            links:[
              // Note that when computing the target endpoints
              // each service-level link produces a new tag...
              // However, this spec allows multiple endpoints to be targets 
              // within the same tag.
              {
                role: rn
                channel: rch
              }
            ]
          }]
      }], -1)
      
    }
  }
}


// Extract: Given a deployment, config its underlying service
//  and extract subdeployments into a solution
//  - First spreads config
//  - Second extract subdeployments
//  - Last: compute connectors.


#Extract: {
  $q=#p: {
    deployment: _//#Deployment
  }
  
  // Auto-convert the deployment of a component to the deployment of its equivalent service app.
  // also, take care of converting between versions of model when passed a legacy deployment
  let $dp = $q.deployment
  
  // let $p = { 
  //   if $dp.artifact.ref.kind == "component" {
  //     deployment: {
  //       artifact: (#ServiceFromComponent & {#p: $dp.artifact}).out
  //       meta: $dp.meta 
  //       name: $dp.name
  //       config: {
  //         parameter: $dp.config.parameter
  //         resource:  $dp.config.resource
  //         resilience:$dp.config.resilience 
  //         scale: detail: {
  //           for r, rd in artifact.description.role {
  //             "\(r)": hsize: $dp.config.scale.hsize
  //           }
  //         }    
  //       }
  //       up: $dp.up
  //     }
  //   }

  //   if $dp.artifact.ref.kind == "service" {
  //     deployment: $dp
  //   }
  // }

  let $p = {
    if $dp.artifact.ref.kind == "component" {
      deployment: (#ServiceFromComponentDeployment & {#p: $dp}).out
    }
    if $dp.artifact.ref.kind == "service" {
      deployment: $dp
    }
  }

  let spread = (#Spread & {#p: $p.deployment}).out
  let subs = (#Deploy & {
    #p: {
      artifact: spread
      meta: $p.deployment.meta
      name: $p.deployment.name
    }}).out


  // links encode the set of links as an array
  links: list.FlattenN([
    for sd, r in subs.Ulink {
      [for sc, rr in r {
        [for td, tr in rr.to {
          [for tc, tm in tr {
            {
              s_d: sd 
              s_c: sc 
              t_d: td
              t_c: tc
              // NOTE: We can leave out the meta, for now, as it is present on the deployments
              meta: tm
            }
          }]
        }]
      }] 
    }
  ],-1)

  // Another way of expressing the lilnks among deployments, which is not exposed
  _linkd: {
    for sd, r in subs.Ulink {
      for sc, rr in r {
        "\(sd)": "\(sc)": list.FlattenN([
          for td, rrr in rr.to {
            [for tch, tm in rrr {
              {
                t_d: td
                t_c: tch
                meta: tm
              }
            }]
          }
        ], -1)
      }
    }
  }

  // NOTE: We are hiding the parent/descendant/leafs structures, as they are not necessary
  //parent: subs.parent
  // descendant: {
  //   for dn, isd in subs.deployments if subs.descendant[dn] != _|_ {
  //     "\(dn)": [for ddn, isd in subs.descendant[dn] {ddn}]
  //   }
  //   for dn, isd in subs.deployments if subs.descendant[dn] == _|_ {
  //     "\(dn)": []
  //   }
  // }
  //leafs: subs.leafs
  top: $p.deployment.name
  deployments: {...}
  cspec: #cspec
  for k, v in subs.deployments {
    deployments: "\(k)": {
      //UNCONFIG config: v.config   
      config: {
        resource: v.config.resource
        scale: v.config.scale
        resilience: v.config.resilience
        parameter: {...}
        if v.top == k {
          parameter: $p.deployment.config.parameter //v.config.parameter
        }
      }
      name: v.name
      meta: v.meta
      //top: v.top
      up: v.up
      // We do not really need to keep the scale, as at the end
      // everything has been spread to the actual atomic roles

      // config: scale: {
      //   let vscale = v.artifact.description.config.scale
      //   if vscale.qos != _|_ {
      //     qos: vscale.qos
      //   }
      //   if vscale.detail != _|_ {
      //     for rn, rv in v.artifact.description.role {
      //       detail: "\(rn)": vscale.detail[rn]
      //     }
      //   }
      // }
      if v.artifact.description.builtin {
        artifact: v.artifact
      }
      if !v.artifact.description.builtin {
        artifact: (#ComputeService & {#p: v.artifact}).artifact
      }
      artifact: {...}
    }
  }
}

#ComputeService: {
  #p: {...}  // ServiceArtifact

  artifact: {
    spec: #p.spec
    ref: #p.ref
    description: {
      let vd = #p.description
      _ccon : #ComputeConnectors & {#p: service: vd}
      config: vd.config
      
      srv: vd.srv
      role: {
        for rn, rv in vd.role {
          "\(rn)": {
            artifact: {#ComputeComponent & {#p: rv.artifact}}.artifact
            name: rv.name
            if rv.meta != _|_ {
              meta: rv.meta
            }
            config: rv.config
          }
        }
      }
      //connect: vd.connect
      //vset: vd.vset
      builtin: vd.builtin
      // fromself: ccon.fromself 
      // toself: ccon.toself
      connector: _ccon.connector
    }
  }
}

#ComputeComponent: {
  #p: {...} // Component artifact

  artifact: {...}
  _artifact: {...}

  let artd = #p.description

  if artd.builtin {
    _artifact: #p
  }
  if !artd.builtin {
    let dpar = (u.#FlattenCUE  & {#p: artd.config.parameter}).#value // For env
    _artifact: {
      ref:  #p.ref
      spec: #p.spec
      description: {
        builtin: false
        srv    : artd.srv
        config : artd.config
        probe  : artd.probe
        profile: artd.profile
        size   : artd.size
        for con, cond in artd.code {
          code: "\(con)": {
            init: cond.init
            size: cond.size 
            image: cond.image
            name: cond.name
            if cond.entrypoint != _|_ {
              entrypoint: cond.entrypoint
            }
            if cond.cmd != _|_ {
              cmd: cond.cmd
            }
            if cond.user != _|_ {
              user: cond.user
            }
            mapping: {
              //filesystem: cond.mapping.filesystem
              filesystem: {
                for fsn, fsd in cond.mapping.filesystem {
                  for fsnf, fsnfv in fsd {
                    if fsnf != "data" {
                      "\(fsn)": "\(fsnf)": fsnfv
                    }
                    if fsnf == "data" {
                      for dfk, dfv in fsnfv {
                        if dfk == "parameter" {
                          "\(fsn)": data: value: {u.#Retrieve & {#p: artd.config.parameter, #k:dfv}}.#value
                        }
                        if dfk != "parameter" {
                          "\(fsn)": data: "\(dfk)": dfv
                        }
                      }
                    }
                  }
                }
              }
              //env: cond.mapping.env
              env: {
                for enn, end in cond.mapping.env {
                  "\(enn)": {
                    for endk, endv in end {
                      if endk == "parameter" {
                        value: "\(dpar[endv])"
                      }
                      if endk != "parameter" {
                        "\(endk)": endv
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  artifact: json.Unmarshal(json.Marshal(_artifact))
}


// Factory for a function that Produces a list of Deployments from a spread-out 
// service application.
// All deployments follow the format expected by the platform.
// In addition, links among deployments are also issued.
// INTERNAL
_#SubdeployF: {
  #next: _ 

  func: {
    $p = #p: { 
      // This is really a Configured Deployment (or role) plus a top
      artifact: {...}
      // User-defined metadata for the service
      meta: {...}
      // Name to be used to refer to the deployment in links
      name: string
      // Name of top deployment
      top: string | * null
      up: string | *null
    }

    // Abbreviations
    let $s = $p.artifact
    let $sd ={ $s.description, role: {...}}

    // The name of the top level deployment
    let $self = $p.name

    // Compute the top deployment for subdeployments
    _top: string
    _up: string | null

    if $p.top == null {
      _top: $self
      _up: null
    }
    if $p.top != null {
      _top: $p.top
      _up: $p.up
    }

    // Path separator
    let $_ = "." 

    // We do not add config, as the spread is done
    // we add the builtin to verify thins are ok
    let copyfieldsd = ["srv", "builtin", "config"] 
    let copyfields  = ["spec", "ref"]

    out: {
      Ulink     : {...}
      parent    : {...}
      descendant: {...}
      leafs     : "\($self)": len(Ulink) == 0


      // Inverse map from roles to vsets, with the metadata
      let rolevsets = {
        for vn, vd in $sd.vset {
          for rn, rnd in vd.roles {
            "\(rn)": "\(vn)": rnd
          }
        }
      }

      // Build a dictionary of connectors to echannels
      let selfchansdict = {
        for lb, cond in $sd.connect {
          for vs, vsc in cond.from if vs != "self" {
            let realvsc = list.FlattenN([vsc], -1)
            for rn, rnd in $sd.vset[vs].roles if $sd.role[rn].artifact.ref.kind == "service" {
              for vsce in realvsc {
                "\(lb)":"\(rn)\($_)\(vsce)": true
              }
            }
          }
        }
      }

      let selfchans = {
        for lb, lbc in selfchansdict {
          "\(lb)": [for lbcn, whocares in lbc {lbcn}]
        }
      }

      // Prepare the top level deployment
      deployments: "\($self)": 
      {
        config: $sd.config
        name: $self
        meta: $p.meta
        top: _top
        up: _up

        artifact: {
          for k in copyfields {
            "\(k)": $s[k]
          }
          description: {
            for k in copyfieldsd {
              "\(k)": $sd[k]
            }

            if !$sd.builtin {
              role: {...}
              // prune vsets: remove the service ones
              // TODO: When vsets get support for channel translation,
              //       need to arrange the inherited self channels
              //       For now, we assume subservices are not part of any vsets
              //       other than their own
              vset: {
                for vn, vnd in $sd.vset {
                  for rn, rnd in vnd.roles if $sd.role[rn].artifact.ref.kind == "component" {
                    "\(vn)": {
                      srv: vnd.srv
                      roles: "\(rn)": rnd
                    }
                  }
                }
              }

              connect: {
                // The connectors survive, although their targets
                //  are transformed
                for cn, cnd in $sd.connect {
                  "\(cn)": {
                    as: cnd.as
                    from: {...}
                    to: {...}
                  }
                  if cnd.from.self != _|_ {
                    if selfchans[cn] != _|_ {
                      "\(cn)": from: self: list.FlattenN([cnd.from.self, selfchans[cn]], -1)
                    }
                    if selfchans[cn] == _|_ {
                      "\(cn)": from: self: list.FlattenN([cnd.from.self], -1)
                    }
                  }
                  if cnd.from.self == _|_ {
                    if selfchans[cn] != _|_ {
                      "\(cn)": from: self: selfchans[cn]
                    }
                  }
                  if cnd.to.self != _|_ {
                    "\(cn)": to: self: cnd.to.self
                  }
                }
              }
            }
          }
        }
      }

      if !$sd.builtin {
        // Process all subservices
        for rn, rd in $sd.role if rd.artifact.ref.kind == "service"  {
          let ssd     = rd.artifact.description
          let subname = "\($self)\($_)\(rn)"

          // Auxiliary top level structures to keep the parent tree for further manipulation
          parent: "\(subname)": $self
          descendant: "\($self)": "\(subname)": true

          // Linked to server endpoints of subservice become client endpoints of enclosing service
          // Also: an inter-service level link is established between top level connectors
          // linked to the server endpoint and the new client endpoint
          for csn, csd in ssd.srv.server if !csd.inherited {
            let echannel = "\(rn)\($_)\(csn)"

            for lb, lrst in $sd.connect {
              // For each vset/channel pair in the link
              //  if the role is part of the vset
              for vn, rncdict in lrst.to if $sd.vset[vn].roles[rn] != _|_ {
                for rnc, rnm in rncdict if $sd.vset[vn].roles[rn].map[rnc] == csn {

                  deployments: "\($self)": artifact: description: {
                    srv: client: "\(echannel)": {
                      protocol: csd.protocol
                      inherited: true
                    }
                    // link: "\(lb)": to: self: "\(echannel)": null
                    connect: "\(lb)": to: self: "\(echannel)": meta: rnm.meta// was null
                    connect: "\(lb)": as: "lb"
                  }

                  Ulink: "\($self)": "\(echannel)": to: "\(subname)": "\(csn)": $sd.vset[vn].roles[rn].meta // the metadata
                }
              }
            }
          }

          // Linked from client endpoints of subservice become server endpoints of enclosing
          // service. with links conducent to the connector the endpoint was linked to
          for csn, csd in ssd.srv.client  if !csd.inherited {
            let echannel = "\(rn)\($_)\(csn)"

            for lb, cond in $sd.connect {
              for vn, vch in cond.from if rolevsets[rn][vn] != _|_ {
                deployments: "\($self)": artifact: description:{ 
                  srv: server: "\(echannel)":  {protocol: csd.protocol, inherited: true}
                  //link: self: "\(echannel)": to:  lrst[csn].to
                  //connect: "\(lb)": from: self: selfchans[lb].from.self
                  connect: "\(lb)": as: "lb"
                }
                Ulink: "\(subname)": "\(csn)": to: "\($self)": "\(echannel)": meta: {} // Was null.. no added metadata              
              }
            }
          }

          let subios = (#next & {
            #p: {
              artifact:  rd.artifact
              meta: rd.meta
              name: subname
              // All subdeployments depend from the topmost independent
              top: _top
              up: $self
            }
          }).out

          deployments: subios.deployments
          Ulink: subios.Ulink
          parent: subios.parent
          descendant: subios.descendant
          leafs: subios.leafs

        }

        // For simple roles, we capture the structure
        for rn,rd in $sd.role if rd.artifact.ref.kind != "service"  {
          deployments: "\($self)": artifact: description: {
            role: "\(rn)": {
              name: rn //COMPATIBILITY
              // Can be more selective in the config 
              // distinguishing builtins, but extra info does
              // no harm.
              artifact: rd.artifact 
              //config: scale: hsize: rd.config.scale.hsize //REVISE IF ALL
              config: rd.config
            }
            vset: "\(rn)": $sd.vset[rn]
          }

          // We now convert the vset connections connections to roles, simplifying the semantics
          // Note that the meta field that appears here is the one to be inserted
          // in the connector field of the output
          for vsn, vsm in rolevsets[rn] {
            for cn, cnd in $sd.connect {
              if cnd.to[vsn] != _|_ {
                for chn, chm in cnd.to[vsn] {
                  deployments: "\($self)": artifact: description: connect: "\(cn)": to: "\(rn)": "\(vsm.map[chn])": {
                    chm
                    meta: {
                      for vsmmk, vsmmv in vsm.meta if chm.meta[vsmmk] == _|_ {
                        "\(vsmmk)": vsmmv
                      }
                    }
                  }
                }
              }
              if cnd.from[vsn] != _|_ {
                deployments: "\($self)": artifact: description: connect: "\(cn)": from: "\(rn)": list.FlattenN([cnd.from[vsn]], -1)
              }
            }
          }
        }
      }
    }
  }
}

// Note, we assume that 100 is a good enough depth for real world cases,
// and it does not introduce a too grievous performance toll
// The result of applying #Deploy is a Solution in which one of the
// deployments is TOP for all the rest
#Deploy: (u.#RecurseN & {#N: 100, #funcFactory: _#SubdeployF}).Func



// Utility function for projecting all fields except those
// provided in "fields"
#NProjectF : {
	$p=#p: {
		ins: {...}
		fields: [...string]
	}
	let ft = or($p.fields)

	out: {
		for k,v in $p.ins {
			if (k & ft) == _|_ {
				"\(k)": v
			}
		}
	}
}