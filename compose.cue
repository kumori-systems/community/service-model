package kumori_exp

#CCContainer: {
  $p=#p: _ // #Container
  $r=#r: _ // Role


  #i: number 
  out: {
    image: $p.image.hub.name + "/" + $p.image.tag
    if $p.entrypoint != _|_ {
      entrypoint: $p.entrypoint
    }
    if $p.cmd != _|_ {
      command: $p.cmd
    }
  }
}

// We start from a spread Service
#ComposeService : {
  $p=#p: _ // _Service

  
  // scale takes care of just that: how many
  // instances there are for each role
  scale: {
    for rn, rnd in $p {
      "\(rn)": $p.config.scale.hsize
    }
  }

  out: {
    version: "3.9"
    services: {
      for rn, rd in $p.role {
        let containers = [for k, kd in rd.artifact.description.code {kd}]
        let main = containers[0]
        let instances = rd.config.scale.hsize

        "\(rn)": (#CCContainer & {#p: main, #i: instances}).out

        for k, cnd in containers if k != 0 {
          "\(rn)_\(cnd.name)": {
            (#CCContainer & {#p: cnd, #i: instances}).out
            network_mode: "service: \(rn)"
          }
        }
      }
    }
  }
}