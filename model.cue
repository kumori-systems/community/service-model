package kumori

import (
	"net"
)

// Prerelease label specification
// It is optionally provided for artifacts
//

#Identifier: "([0-9A-Za-z-]+)"

// If it starts with a 0, it must contain some non-digit character
#Ident0:  "(0[0-9A-Za-z-]*[A-Za-z-][0-9A-Za-z-]*)"
// If it does not start with a 0 it can contain anything
#Identn0: "([1-9A-Za-z-][0-9A-Za-z-]*)"

// A valid label identifier
#Lidentifier: "(" + #Ident0 + "|" + #Identn0 + ")"
// A valid label is a dot-separated sequence of label valid identifiers
#Label: #Lidentifier + "(\\." + #Lidentifier + ")*"

// A valid build metadata value is a dot separated sequence of valid identifiers
#Build: #Identifier + "(\\." + #Identifier + ")*"

// A prerelease string is composed of a label, optionally followed by
// a + and a valid build metadata string
#Pre: =~ ("^" + #Label + "(\\+" + #Build + ")?$")

// Use the type provided in the net package to constrain
// names of channels.
// This makes sure that names fit within the specification of a FQDN
// We need to further constrain the total length to accomodate
// our prefixes ("0...??", set, ready). Being conservative, we allow for 1000 tags
// in which case we need to reserve 10 characters out of the 255
#DomainName: net.FQDN
#DomainName: =~ "^.{0,245}$"

#Version: 3*[uint]
#Semver: {
	version       : #Version
	prerelease?   : =~ #Label
	buildmetadata?: =~ #Build
}

// Move the spec up in major as it is incompatible
#Spec: [1,0] 

#BaseArtifacts: {
	component: _BaseComponent
	service:   _BaseService
}

#Artifacts: {
	component: _Component
	service:   _Service
}

#artifacts_a:  [for k, v in #Artifacts {k}]
#artifacts:  or(#artifacts_a)

// Versioned artifacts in Kumori's model
// Currently, only service and component artifacts
// Full identifier of an artifact will be the concatenation of
// kumori://<domain>/<module>/@<major>.<minor>.<patch>-<prerelease>+<buildmetadata>/<name>:<kind>
// NOTE that "name" may be empty, in which case the ":<kind>" suffix follows the version string
// immediately, without "/"
// NOTE: Local modules substitute the version string above for "/@local"
// NOTE also that local modules cannot be distributed! (i.e., when published, they cease to be local)
// Import path is the above minus the "kumori://" prefix
// Note that the <module> part in the above identifier should coincide
// with the name of the module used to distribute the artifact (under the <domain>
// namespace of the distribution channel)
//
#Artifact: {
  spec: #Spec
  ref: {
    kind  : #artifacts
    domain: string
	module?: string
    name  : string
	local?: bool | * false
    #Semver
  }

  description: {
	  builtin: bool | * false
	  if builtin {
		  #BaseArtifacts[ref.kind]
	  }
	  if !builtin {
	  	#Artifacts[ref.kind]
	  }
  }
}

#BaseArtifactDescription: {
	srv:    #MicroService
	config: #Configurable & {
	  scale: #servers: srv.#servers
	}
}

#Builtin: {description: {builtin: true, ...},...}

#ServiceArtifact: #Artifact & {
	ref: kind: "service"
}

#ComponentArtifact: #Artifact & {
  ref: kind: "component"
}


// In this version we only have resources usable from within deployed services:
//  - Volatile volumes
//  - Persistent volumes
//  - Secrets, with unspecified type
//  - Domains (http-inbounds)
//  - Certificates (http-inbounds)
//  - CA (http-inbounds)
//  - Ports (tcp-inbounds)
#Resource: #Volume | #Secret | #Port | #Domain | #Certificate | #CA

// Volumes can be volatile or persistent
#Volume: #Volatile | #Persistent

#Units: {
	storage: "k" | "M" | "G" | "T" | "P" | "E" | "Ki" | "Mi" | "Gi" | "Ti" | "Pi" | "Ei"
	cpu: "m" | ""
	ram: "G" | "M" | "Gi" |  "Mi"
	bandwidth: "G" | "M" | "Gi" | "Mi"
}

#UnitKinds_a: [for k, v in #Units {k}]
#UnitKinds: or(#UnitKinds_a)

#ResourceAmount: {
	kind: #UnitKinds
	size: number & >=0
	unit: #Units[kind]
}


// Resources directly described (no need to register them, 
// but may be subject to some sort of quotes), like Volatile volumes
#Volatile: volume: #ResourceAmount & {kind: "storage"}

// Resources accessible through a string ID registered
// within the stamp. The ID must be provided
#Persistent: volume: string
#Secret:     secret: string
#Port:       port: string
#Domain:     domain: string
#Certificate: certificate: string
#CA:          ca: string

#DataResourceKey: "secret" | "port" | "domain" | "certificate" | "ca"
#ResourceKey: #DataResourceKey | "volume"

#DataResource: [#DataResourceKey]: string

#ResourceTree: [string]: (#Resource | #ResourceTree)

// A channel, for the time being, declares only its protocol
// SPEC: determine when http/grpc if a spec of the API should be included
// for helping with composition
// inherited is true only when reaarranging subservices
#Channel: {
	inherited: *false | bool
	protocol: "udp" | "tcp" | *"http" | "grpc"
}

// A server can declare the port where it wants to listen.
// If not set it will take port 80 by default.
// Code in a component does not use the name for any kind of resolution
// The component spec can make use of the port to configure the software
// by various means (env, config file,...)
// SPEC: When an explicit API for the platform is implemented, it can be consulted to
// resolve other interesting facts about the channel.
//
// Note that we can specify a range of ports to be associated with a channel
// by specifying a portmax field greater than port. The range is the set of ports
// between port and portmax (inclusive).
// Such declaration will enable all clients of the channel to be able to reach
// all the ports in the range. By default, the range  includes only the port.
#Server: {
	#Channel
	port:    uint16 | *80
	portnum: (>=1 & < (65535 - port)) | *1
}

// A Client channel is a bare channel. No port is specified
// It represents a depndency that should exist.
// The name given to the client channel will be used to resolve the
// servers behind it.
// SPEC: should include an indication of optionality for binding it?
#Client: #Channel


// Scaling specification (recursive)
// It is a struct with either a qos specification per server channel, a detailed 
// specification per service role, or an hsize field imposing a number of instances 
// of a component.
// When the artifact is a component, only the hsize field can and must be provided.
// If not a component, and a detailed spec can provided, or, alternatively, a 
// qos spec can be provided.
#ScaleSpec: {
  #servers: string 
  #roles:   string
  
  // Valid for service apps and all kinds of roles (both service and component)
  // 
  // Simple collection of (response time,rate of arrival)  pairs. One per server channel
  // This specifies the requiremets of QoS, AND, the expected maximum rate of arrival.
  ["qos"]: [#servers]: #QoSSpec

  // Only valid for service roles/service apps
  //
  // Structure with all roles/subroles potentially represented.
  // 
  // In order to use this spec properly, one needs to know
  // the struture of the service under deployment.
  //
  // It is advised that for each component role, a rate of 100
  // equates the minimum number of instances requied to satisfy
  // the resilience requirement
  ["detail"]: [#roles]: #ScaleSpec

  // MUST be used by (and it is only valid for) component roles
  ["hsize"]: uint
}


// A Configurable represents any entity capable of acquiring configuration
// by means of parameters or resources
//
#Configurable: {
	parameter:  [string]:  _             // Arbitrary values are allowed
	resource:   [string]: #Resource //#ResourceTree      // Arbitrary resources are allowed
    resilience: uint | *0 // For components only 0 allowed
	scale: #ScaleSpec // For components, only qos allowed
  
	#parameters_a: [ for k, v in parameter { k } ]
	#parameters:   or(#parameters_a)
	#resources_a:  [ for k, v in resource { k } ]
	#resources:    or(#resources_a)
	#volumes_a:    [for k, v in resource if v.secret == _|_ {k}]
	#volumes:      or(#volumes_a)
	#secrets_a:    [for k, v in resource if v.secret != _|_ {k}]
	#secrets:      or(#secrets_a)
}

// A MicroService is just a bunch of dictionaries with structural and configuration
// information.
//
// In addition, it enumerates the sets of keys for each dictionary as types
#MicroService: {
	client: [cn=string]: #Client // Plain dictionary of Client channels
	server: [string]: #Server // Plain dictionary of Server channels
	duplex: [string]: #Server // Plain dictionary of Duplex channels

	// Instead of explicitly enumerating each set of keys
	// we can simply compute them as types like this

	#clients_a:  [ for k, v in client { k } ]
	#clients:    or(#clients_a) & #DomainName
	#servers_a:  [ for k, v in server { k } ]
	#servers:    or(#servers_a) & #DomainName
	#duplexes_a: [for k, v in duplex { k } ]
	#duplexes:   or(#duplexes_a) & #DomainName
}

// A flat service is either a builtin, or all its roles are Atoms.
#FlatServiceArtifact: this = (#ServiceArtifact & {
	if this.description.builtin == false {
		description: role: [string]: artifact: #AtomArtifact
	}
})

// AtomArtifact is when it is a component or a builtin
#AtomArtifact: this = (#Artifact & {
	if this.description.builtin == false {
		ref: kind: "component"
	}
})
