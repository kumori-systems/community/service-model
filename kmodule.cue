package kmodule

{
	domain: "kumori.systems"
	module: "kumori"
	version: [
		1,
		1,
		6,
	]
	cue: ">=0.4.2"
	spec: [
		1,
		0,
	]
	dependencies: "kumori.systems/utils": {
		query:  "latest"
		target: "kumori.systems/utils/@1.1.1"
	}
	sums: "kumori.systems/utils/@1.1.1": "/C/tSKwSG773e6g9bBGcOtiuYh5SymoW/jDsb/q3lFM="
	artifacts: {}
}
