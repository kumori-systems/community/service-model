package kumori 


// Function that converts a spec [1,0,0] component artifact
// and converts it to a v. [2,0,0] artifact
// NOTE: MAY NEED ADDITION OF EXTRA FEATURES PLUS TESTING
#ConvertComponent: {
	#p: {
		spec: [1,0,0]
		ref: {kind: "component",...}
		description: {...}
	}
	out: {
		spec: [2,0,0]
		ref: #p.ref
		description: _Component & {
      let $cd = #p.description 
      srv: $cd.srv
      config: $cd.config
      code: $cd.code & {
        for k, v in $cd.code {
          "\(k)": size: { // TODO: revise unit conversions
            cpu: $cd.size.$_cpu
            memory: $cd.size.$_memory
          }
        }
      }
      probe: $cd.probe
      profile: {
        $cd.profile
        iopsintensive: $cd.size.$_iopsintensive
      }
      size: bandwidth: $cd.size.$_bandwidth
    }
	}
}


// Function that takes a spec [1,0,0] Service description
// and converts it to a spec [2,0,0] Service description
// No recursion!!!
#ConvertService: {
  #p: {
    spec: [1,0,0]
    ref: {kind: "service",...}
    description: {...}
  }

  let $p = #p.description

  out: {
    spec: [2,0,0]
    ref: #p.ref
    description: {
      config: $p.config
      srv:    $p.srv

      // Role conversion
      for rn, rd in $p.role {
        role: "\(rn)": {
          name: rn 
          meta: *{} | rd.vset[rn]
          artifact: rd.artifact 
          config: rd.cfg
          resilience: rd.rsize.$resilience
        }
        for vn, vm in rd.vset {
          vset: "\(vn)": roles: "\(rn)": vm
        }
        vset: "\(rn)": {
          roles: "\(rn)": role[rn].meta
          srv: rd.artifact.description.srv
        }
      }

      // Connector conversion
      for cn, cd in $p.connector {
        connector: "\(cn)": as: cd.kind
      }

      // link conversion
      for l1, lr1 in $p.link {
        for l2, lr2 in lr1 {
          for l3, l4 in lr2 {
            link: "\(l1)": "\(l2)": "\(l3)": "\(l4)": null
          }
        }
      }
    }
  }
}

// Converts a given deployment
#ConvertDeployment: {
  #p: _
  out: {
    name: #p.ref.name 
    meta: {legacy: true, ref: #p.ref}

    up: null
    config: {
      parameter: #p.description.configuration.parameter
      resource:  #p.description.configuration.resource
      scale: detailed: hsize: {
        for k, v in #p.description.configuration.hsize {
          "\(k)": v.$_instances
        }
      }
      resilience: 0 // Arbitrario, pero debe ser inane.
    }

    artifact: (#ConvertService & {#p: #p.description.service}).out

  }
}

