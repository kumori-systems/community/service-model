// Code depends on target cluster version (cspec)
// The version is selected with njection flags: cspec1.0, cspec1.1,...
// Injection flags. They select for the version of the cluster spec
// When it is the default, the first version is the one for which the code
// is provided, the rest are negatives of the versions handled, anded among themselves
@if(cspec10||!cspec10)
package kumori

#cspec: string | *"1.0" @tag(cspec,short="1.0")

// A solution is a set of configured deployments and a set of links among them
// Deployments may hold a relationship (an up relation) among themselves or not 
// Deployments may also refer to built-in service apps (e.g., inbound services)
// If a deployment D has a top relationship with deployment T within the solution
// T's destruction involves D's destruction.
//
// A solution is the format accepted by kumori cluster API to launch or upgrade
// a deployed compund service.
//
// A solution is formed by multiple deployments, each being the result
// of activating a service consisting of multiple component roles.
//
//
#Solution: {
  // make it optional until admission admits it.
  cspec: #cspec
  deployments: [dn=string]: #ClusterDeployment & {name: dn}// Already spread.
  links: [...#DLink]
  top: string
  // NOTE: This forces exactly one deployment has the name of the solution
  for k, v in deployments if v.up == null {
    top: k
  }
}

// Very simple structure specifying a link between deployments
// s_  source of link (d: deployment, c: channel)
// t_  target of link
// The deployment names must come from the dictionary of deployments in the solution
#DLink: {
    s_d: string
    s_c: string
    t_d: string
    t_c: string
    meta: {...}
}

// NOTE: This is not properly working as designed. Probably there is a mismatch in the built
// Solution that makes this fail.
#ClusterDeployment: #PreDeployment & {
  artifact: #OutServiceArtifact
}

// Should constrain 'artifact' to be an "Artifact", but for some reason, performance is shot.
// This implies that if a non-artifact is passed, the rrors will be produced downstream
// and will be more difficult to pinpoint.
// 
// The constrain imposes a performance penalty, especially for services with vsets
#Deployment: #PreDeployment //& {artifact: #Artifact}

#PreDeployment: #Role
#OutServiceArtifact: {...}

_#OutServiceArtifact:  this = {
  _BaseService
  // The config used in a deployment. Includes resources, parameters and scaling
  config:  scale: #roles:  this.#roles

  srv:    #MicroService & {duplex: {}}  // The channel interface, no duplex channels allowed
  //connect: {...}

  // A Role is a Deployment. 
  // The metadata of the deployment is the metadata associated with its
  // default vset
  role: [rn=string]: #PreDeployment & {
    artifact: #ComponentArtifact
    name: rn
    
    // Automatically capture the scale spec if detailed
    // This makes sure to include the detail hsize if this is a component role
    //   and such detail has been provided

    config: {
      if this.config.scale.detail != _|_  { 
        if this.config.scale.detail[rn] != _|_ {
          scale: this.config.scale.detail[rn]
        }
      }
      scale: hsize: > config.resilience | 0 
    }
  }

  connector: [string]: {
    kind: "lb" | "full"
    clients: [...{
      role: #roles
      channel: this.role[role].artifact.description.srv.#clients
    }]
    servers: [...{
      meta:  [...{
        auto: {...}
        user: {...}
      }]
      links: [...{
        role: #roles
        channel: this.role[role].artifact.description.srv.#servers | this.role[role].artifact.description.srv.#duplexes
      }]
    }]
  }

  #roles_a: [for k, v in role {k}]
  #roles: or(#roles_a)

  #atoms_a: [for k, v in role if v.artifact.ref.kind == "component" {k}]
  #atoms: or(#atoms_a)
  
}