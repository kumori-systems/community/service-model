package kumori

// Schema for SLA definition

// SLA specification has two parts
// 1. A severity model defining the impact
//    of QoS violations
// 2. A set of QoS specs, in the form of a list of latency/load ranges
//
// Given a Deployment, its configured SLA on first deployment is part
// of the deployment manifest. The SLA can be changed through the life of
// the deployment through the deployment management API
//
#SLA: {
	severity: #Severities | *"none"
	ranges: [...#QoSSpec]
}

// The dictionary, SeverityModel, contains all models
// that can be handled by the platform.
// Each model specifies the parameters that can be defined
// by the deployer.
//
// There is an implicit "none" model which, essentially, applies no penalties
// to QoS violations. This is the model applied by default.
//

#SeverityModel: {
	basicLinear: {
		interval: int | *100
		penalty:  int | *1
	}
}

// The type of all available severity models
#Severities: or([ for k, v in #SeverityModel { k } ])


#QoSSpec: {

	// A "rate of requests" measure as an integer. This is roughly equivalent
	// to: 100, is a base line powerful deployment
	//     200, is like having the power of two deployments at the same time
	//       ...
	rate:         uint 

	// The maximum acceptable response time for the given rate. In ms
	responsetime: uint 
}
