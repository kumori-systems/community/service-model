package kumori

import (
	"list"
//	u "kumori.systems/utils:utils"
)

#DefaultContainerSize: {
	cpu:    {kind: "cpu", size: 500 |*500, unit: "m"}
	memory: {kind: "ram", size: 100 |*100, unit: "M"}
}

#DefaultComponentSize: {
	bandwidth: {size: 10 |*10, unit: "M"}
}
// Component size specification in manifests, with units
// preset
#ComponentSize:  {
	bandwidth:    #ResourceAmount & {kind: "bandwidth"}
	minbandwidth: *bandwidth.size | (number & >= 0 & <= bandwidth.size)
	mincpu:       #ResourceAmount & {kind: "cpu"} // Should be at least the sum of the mincpu's of the containers
}

// The container size specifies CPU and memory. Note, though, that
// only min is specified for CPU. 
// Memory needs are respected/met
#ContainerSize: {
	cpu:    #ResourceAmount & {kind: "cpu", unit: _ | *""}   
	mincpu: *cpu.size | (number & >= 0 & <=cpu.size) // Same Units as cpu
	memory: #ResourceAmount & {kind: "ram"}   
}

// Currently only threadability is understood in the profile. In the future
// ability to use specialized resources (e.g. CUDA) will be included
//
#Profiles: {
	threadability: *"*" | uint
	iopsintensive: *false | bool
}

// Description of a component
_Component: {

  builtin: bool | *false

	// Common microservice elements: channels, resources, parameters
	// With an extension of duplex channels only for components

	config: #Configurable & {
		scale: { 
			#servers: srv.#servers
		}
		// Note that the resilience is 0 for an instance of a component.
		resilience: 0
	}

	srv:    #MicroService
	code: {...}

	if builtin {
			// The code block should be empty for a builtin
		code: {}
	}

	if !builtin {
		_flat: {...}
		#dpar: {...}
		_rt: {...}
		// The resource paths enabled
		// if len(config.resource) > 0  {
		// 	let rtags = {u.#FlattenTAGS & {#p: config.resource, #k: #ResourceKey}}.#value
		// 	_rt:    rtags.t
		// 	_flat:  rtags.T
		// }
		// if len(config.parameter) > 0 {
		// 	#dpar: (u.#FlattenCUE  & {#p: config.parameter}).#value // For env
		// 	// Although it does not verify index existence up front, it is verified when transforming
		// 	_flat: ["parameter"]: string//or([for k, v in _dpar {k}])
		// }
		

		// A FileMap extracts the contents of a configuration
		// parameter whose reference path is given into
		// the file whose path is specified in the map
		//
		// Property "restart" indicates wether the component needs to be restarted
		// when the mapping is updated by the platform.
		// "text" will output raw text when having a string, otherwise,
		// reverts to json

		#FileMap: {
			mode:   uint16 | *0o644
			rebootOnUpdate: bool | *false
			// Make sure that only one option is used for data
			_datafields : < 2 & len(data)
			data:   {
				[string]: _
				["value"]: _ // Usually, jsonable
			}

			format: *"text" | "json" | "yaml" | "flatdict" 

			if (data.value == _|_ && data.parameter == _|_ ) {
				// Specify formats only when passing jsonables
				format: "text" 
			}
		}
		

		#FSMap: {
			path: string
			#FileMap 
			#VolumeMap
		}

		// A folderMap mounts either a tree
		// of foldermaps or filemaps, or a volume
		// referenced by a string path into the resources
		// of the component (of type Persistent/Volatile)
		// Note that paths in the tree are relative to the root in the foldermap
		//
		#VolumeMap: {
			["volume"]: _ | *""//_rt.volume
			// This field only serves to validate the volumeref
			// NOTE: This validation will have to wait till new cue features arrive.
			// _valid: config.resource[volumeref] & #Volume
		}

		// An EnvMap is a dictionary entry relating env variables to
		// their values expressed as content. 

		#EnvMap:  {["value"]: string, [string]: string}


		// A Mapping is, essentially a function that takes
		// the instantiated configuration of a role/component
		// in a deployment, and produces all artifacts needed
		// by the code running within a container, to be placed within it
		// through standard mechanisms of inserting files, mounting
		// volumes or setting environment variables (to be consumed by the
		// process initiated in the container)
		//
		// This is provided by the developer/integrator composing a Component spec.
		//
		#Mapping: {
			filesystem: [path_=string]: #FSMap & {path: path_}
			env: [string]: #EnvMap
		}

		#Hub: {
			name:   string
			secret: string | *""
		}

		#Image: {
			hub:  #Hub | * {name: "", secret: ""}
			tag:  string
			sha?: string
		}

		// Spec for a container in a code spec
		// for a component.
		#Container: {
			size: #ContainerSize
			name:  string
			image: #Image
			entrypoint?: [...string]
			cmd?: [...string]
			user?: {
				userid:  uint16
				groupid: uint16
			}
			mapping: #Mapping
		}


		// Kinds of probes a component may support.
		// At this moment, just liveness (ping-like)
		// and prometheus-API metrics
		//
		// Note we do not introduce a "readyness" probe per se.
		// An initial readyness info is useful to compute the total
		// provisioning time for an instance, however, the way it seems to be used
		// within kubernetes is not conducent to proper design of a distributed
		// system.
		//
		// We believe that, given that proper elastic actions need other metrics, 
		// readyness could be supplied through the pmetrics probe for the component.
		//
		//
		#ProbeKinds: "liveness" | "readiness" | "pmetrics" 

		#ProtocolParameters: {
			http: {port: uint16, path: string}
			tcp:  port: uint16
			exec: path: string
		}
		// None for the tcp probe, an http path for http-based probes
		//
		#ProbeAttributes: {
			liveness: {
				protocol: [proto="http"|"tcp"|"exec"]:#ProtocolParameters[proto]

				// Only one of the following fields should be provided
				startupGraceWindow?: {
					unit: "attempts" | "ms"
					duration: uint
					probe: bool | *true  // Is it OK to probe during this window and finish it ASAP?
				}

				// Determines the frequency the system probes
				frequency: "high" | "medium" | "low" | uint // in ms

				// TOTAL failure timeout
				timeout?: uint // in ms
			}

			readiness: {

				protocol: [proto="http"|"tcp"|"exec"]:#ProtocolParameters[proto]

				// Note that it does not make sense to have a grace period beyond the one for liveness
				// It is assumed that when a liveness probe answers positively, the instance of the 
				// component is ready, and before that it is not.
				//
				// startupGraceWindow?: {
				// 	unit: "attempts" | "ms"
				// 	duration: uint
				// }


				// Determines the frequency the system probes
				frequency: "high" | "medium" | "low" | uint // in ms

				// TOTAL failure timeout
				timeout?: uint // in ms

			}

			// For pmetrics, assume only http protocol
			pmetrics: {
				protocol: http: #ProtocolParameters.http
			}  // assume only http
		}




		// ============
 
		// Vertical resources for the component. 
		//
		// It MAY have references to the deployment field: derived concrete values out
		// of higher level characteristics, including required response time
		// for a particular load.
		size: #ComponentSize & {
			let sizes = [for k,cc in code {cc.size.mincpu}]
			let summin = list.Sum(sizes)
			mincpu: size: >= summin | * summin
			mincpu: unit: and([for k, cc in code {cc.size.cpu.unit}]) // This forces all CPU units used to be the same
		}

		// Profile for the component.
		// It informs of the relevant characteristics of the computation
		// it carries out. It MUST be concrete
		profile: #Profiles

		// Probes this component implements.
		// A probe is an IP port the platform can contact for different purposes
		// This is a path for dynamic interaction of platform and running
		// component instance.
		//
		// Note that we must specify which container implements each probe.
		// For probes that allow it, several containers may be provided
		// At this point in time, only the livenes/readines probes admit more than one container
		//
		// Probes ought to be probably on different ports
		// NOTE: all containers share the same network namesapce, and, thus, ports are shared.
		//
		probe: [#containers]: [pk=#ProbeKinds]: #ProbeAttributes[pk]

		// The actual implementation of the component, with mappings to artifacts
		// the component's code can access directly (files, volumes, environment variables)
		// It MAY have references to the config, profile, or size
		//
		code: [cn=string]: #Container & {name: cn}

		// Utility collection of all containers in the component.
		#containers_a: [for k,v in code {k}]
		#containers: or(#containers_a)
	}
}
