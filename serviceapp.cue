package kumori

#Role:  {
  // WARNING: Eliminating this restriction decreases processing time 
  //          by two orders of magnitude!!!
  // TODO: re-insert the restriction when CUE improves its performance
  artifact: _ //#Artifact

  // this should be further constrained by the artifact
  // definition

  name: string
  meta: {...}

  // Restrict the admissible configuration
  config: #Configurable

  // Force the specification of an hsize for Component deployments
  if artifact.ref.kind == "component" {
    config: scale: hsize: uint
  }
  
  up: string | *null
  // Purely informative and totally optional
  // to be used to further automate scaling decissions based on SLA
  ["sla"]: #SLA
}

// A service is a complex structure specifying smaller microservices
// as Roles linked via connectors through their channels.
// It represents a complete SaaS application whose life cycle gets managed as a whole
// when deployed.
//
// This structure describes a Service Artifact
//
//_Service: _PreService
_Service: this = {
  _BaseService
  // The config used in a deployment. Includes resources, parameters and scaling
  config:  scale: #roles:  this.#roles

  srv:    #MicroService & {duplex: {}}  // The channel interface, no duplex channels allowed
  //connect: {...}

  // A Role is a Deployment. 
  // The metadata of the deployment is the metadata associated with its
  // default vset
  role: [rn=string]: #Role & {
    artifact: _ // to refer to it
    name: rn
    
    // Automatically capture the scale spec if detailed
    // This makes sure to include the detail hsize if this is a component role
    //   and such detail has been provided

    config: {
      scale: #servers: artifact.description.srv.#servers
      if this.config.scale.detail != _|_ && this.config.scale.detail[rn] != _|_ {    
        scale: this.config.scale.detail[rn]
      }

      if artifact.ref.kind == "service" && !artifact.description.builtin {
        scale: #roles: artifact.description.#roles
      }

      if artifact.ref.kind == "component" {
        scale: hsize: > config.resilience | 0 // 0 is a special value. It flags a role as not active
      }
    }
  }
  
  // The vsets in a service application.
  //  they specify explicitely the set of roles, ant their metadata
  //  Note that the same role can belong to multiple vsets
  vset: [string]: {
    srv:   #MicroService    // The collection of version sets
    roles: [rn=#roles]: {
      meta: {...} 
      // Force the mapping to b e produced, taking defaults if needed
      for sn in srv.#servers_a {
        map: "\(sn)": role[rn].artifact.description.srv.#servers  | *sn
      }
      for dn in srv.#duplexes_a {
        map: "\(dn)": role[rn].artifact.description.srv.#duplexes | *dn
      }
    }
  }

  // The following loop makes sure each role contributes its own
  // vset
  for rn, rd in role { 
    vset: "\(rn)": {
      srv: rd.artifact.description.srv //NOTE!!!! This implies that the topology of an artifact is not modified by configuration
      roles: "\(rn)": meta: rd.meta
    }
  }

  // Link declarations. There are two kind of links
  // links from client channels to connectors, and links from connectors
  // to either server channels or duplex channels
  // Note that source/targets are vsets.
  // Note also that the value of a target is metadata that gets prepended to that of a vset
  connect: [string]: {
    as: *"lb" | "full"
    // NOTE: we restrict the from vsets to those corresponding to roles
    from: [vsn=#roles]: vset[vsn].srv.#clients | [vset[vsn].srv.#clients,...vset[vsn].srv.#clients]
    // NOTE that when a client reaches a duplex, the duplex acts as a server
    // NOTE2: We are forced to this expression due to some CUE spurious behavior
    to:   [vsn=#vsets]: [sr=(vset[vsn].srv.#servers | vset[vsn].srv.#duplexes)]: {
      // NOTE: The meta value indicated here will merge with the meta value
      //       coming from the vset/role
      //       In case of conflict (same keys) the value provided here prevails.
      meta: {...}
      // EXPERIMENTAL: indices to the range of the target
      // TODO:  Needs to be worked out here (limits for targetnum according to comment, and
      //        in computeconnectors, to break out the semantics, as each target port specifies
      //        a different LB connector)
      let size = vset[vsn].srv.server[sr].portnum - targetbase
      targetbase: uint | *0
      targetnum: (>= 1  & <= size) | *1
    }
            
    if as == "lb" {
      if len(srv.server) != 0 {
        from: ["self"]:     srv.#servers | [srv.#servers,...srv.#servers]
      }
      if len(srv.client) != 0 {
        to:   ["self"]:     [srv.#clients]: {meta: {...}, ...}
      }
    }
  }


  // Computed sets. We compute the list of string for each,
  // and then derive the actual or-ed type
  #vsets_a: [for k, v in vset {k}]
  #vsets: or(#vsets_a)
  #roles_a: [for k, v in role {k}]
  #roles: or(#roles_a)

  #atoms_a: [for k, v in role if v.artifact.ref.kind == "component" {k}]
  #atoms: or(#atoms_a)
  #subservices_a: [for k, v in role if v.artifact.ref.kind == "service" {k}]
  #subservices: or(#subservices_a)
  #connectors_a: [for k,v in connect {k}]
  #connectors: or(#connectors_a)
  #lbs_a: [ for k, v in connect if v.as == "lb" { k } ]
  #lbs: or(#lbs_a)
  #fulls_a: [ for k, v in connect if v.as == "full" { k } ]
  #fulls: or(#fulls_a)
  
}


_BaseService: {
  // The config used in a deployment. Includes resources, parameters and scaling
  config:  #Configurable & {
    scale: #servers: srv.#servers
  }
  srv:    #MicroService & {duplex: {}}  // The channel interface, no duplex channels allowed  
}